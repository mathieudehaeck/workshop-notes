# Ionoc Framework Notes

## Ionic cli

Start new project:
```
$ ionic start [name of app] [url or name of template]
```

Serve app:
```
$ ionic serve
```

Show application in different platforms:
```
$ ionic serve -l
```

