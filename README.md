# Workshop Notes

- [Design for web & mobile](https://bitbucket.org/mathieudehaeck/workshop-notes/src/767058a048d50da74dd0f832b5061a8b3ceb2e0a/Design%20for%20web%20&%20mobile.md?at=master)
- [Front-end development](https://bitbucket.org/mathieudehaeck/workshop-notes/src/767058a048d50da74dd0f832b5061a8b3ceb2e0a/Front-end%20development.md?at=master)
- [Typo3 6.2.0 LTS & speciality plugin documentation](https://bitbucket.org/mathieudehaeck/workshop-notes/src/767058a048d50da74dd0f832b5061a8b3ceb2e0a/Typo3%206.2.0%20LTS%20&%20speciality%20plugin%20documentation.md?at=master)
- [Ionic Framework notes](https://bitbucket.org/mathieudehaeck/workshop-notes/src/9ad6b785d10f8b41f5f06f32bcf40d630fe5605c/ionic-framework-notes.md?at=master)