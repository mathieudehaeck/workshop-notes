Front-end development
=====================
Workshop by Mathieu De Haeck

![Header](https://bytebucket.org/mathieudehaeck/workshop-notes/raw/2682cb0a7ece024514cc19bc03a13789ae153312/img/header.png)
***

#Topics

- The first steps
    * Unix and the command line
    * Hidden files & Asepsis
    * Bonus!
- Set up your development environment
    * Xcode
    * Homebrew
    * NodeJS
- Git
    * git platforms
    * git source control with the terminal
    * git clients
- Ruby Gems
    * sass/scss
    * compass & bourbon
    * sass/compass watchers & alternatives
    * Guard & live reloading
- Node modules
    * Bower
    * GruntJS
    * GulpJS
    * Yeoman
    * Yeoman generators
- Extra
    * PHP storm 8 Tips & Tricks
    * Where can i find all the node and ruby stuff on my computer ?
    * Online tutorials
    * Links
    
***

#The first steps
1. Get familiar with the Unix and the command line
2. Show your hidden Files
3. Asepsis

###Unix and the command line

Change directory:
```
$ cd your/path
```

Change directory one level up
```
$ cd ../
```

Change directory to root (home) folder
```
$ cd
```

Create new folder:
```
$ mkdir myNewFolder
```

Show path of:
```
$ wich
```

Show progress working directory:
```
$ pwd
```

Set filename(s) in directory to lowercase:
```
$ for f in *; do mv "$f" "`echo $f | tr "[:upper:]" "[:lower:]"`"; done
```

Overwrite rights on directory:
```
$ chmod 777 -R ./
```

Set rights to rwxr-xr-x:
```
$ chmod 755 foldername
```

Check rights of directory:
```
$ ls -lah
```

Check rights of files:
```
$ li -l
```

Create new file:
```
$ touch myNewFile
```

Remove file/directory:
```
$ rm –Rf
```

Open application:
```
$ open -a nameOfApplication
```

Previous command:
```
$ !!
```

####Bonus!
Empty space in dock:
```
$ defaults write com.apple.dock persistent-apps -array-add '{"tile-type"="spacer-tile";}'
$ killall Dock
```
Banner:
```
$ banner yourBannerName
```

Say:
```
$ say
$ Hello, this is your mac speaking to you
```

Watch the original Star Wars Episode IV in ASCII:
```
$ telnet towel.blinkenlights.nl
```

Play Tetris, Pong, and Other Retro Games:
```
$ emacs
```
hit `Function` + `F10`, then `t`, then `g`.
Select the game from the list by using arrow keys or by hitting a key corresponding to the game, `S` for Solitaire, `T` for Tetris, t for Hanoi, s for Snake, etc

Other fun stuff:

- [http://www.tecmint.com/20-funny-commands-of-linux-or-linux-is-fun-in-terminal/](http://www.tecmint.com/20-funny-commands-of-linux-or-linux-is-fun-in-terminal/)
- [http://www.binarytides.com/linux-fun-commands/](http://www.binarytides.com/linux-fun-commands/)

###Show/hide hidden files

Show:
```
$ defaults write com.apple.finder AppleShowAllFiles -boolean true
$ killall Finder
```

Hide:
```
$ defaults write com.apple.finder AppleShowAllFiles -boolean true
$ killall Finder
```

###Asepsis
Official site: [http://asepsis.binaryage.com/](http://asepsis.binaryage.com/)

#Set up your development environment
1. Make sure you've got the latest version of Xcode
2. Install Homebrew
3. Install NodeJS and NPM
4. It's recommended that you install the latest version of Php Storm (8)

### Xcode
[Download Xcode](https://itunes.apple.com/us/app/xcode/id497799835?ls=1&mt=12)

### Homebrew (The missing package manager for OS X)
Official site: [http://brew.sh/](http://brew.sh/)

Install Homebrew:
```
$ ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

Using Homebrew:
```
$ brew install wget
```

Help:
```
$ brew -h
```

***

###NodeJS
Official site: [http://nodejs.org/](http://nodejs.org/)

Install using installer package: [http://nodejs.org/download/](http://nodejs.org/download/)
  

Install using homebrew: [http://thechangelog.com/install-node-js-with-homebrew-on-os-x/](http://thechangelog.com/install-node-js-with-homebrew-on-os-x/)

Check if npm is installed:
```
$ npm -v
```

Initialize:
```
$ npm init
```

Install packages using npm:
```
$ npm install --save-dev nameOfPackage
```

Uninstall packages:
```
$ npm uninstall --save-dev nameOfPackage
```

Update:
```
$ npm update
```

***

#Git

- [Git official site](http://git-scm.com/)
- [Git documentation](http://git-scm.com/doc)
- [Github](https://github.com/)
- [Bitbucket](https://bitbucket.org/)
- [Markdown documentation](http://daringfireball.net/projects/markdown/)
- [Markdown Demo](https://bitbucket.org/tutorials/markdowndemo)

###Github

Create a new repository on the command line:

```
$ touch README.md
$ git init
$ git add README.md
$ git commit -m "first commit"
$ git remote add origin https://github.com/MathieuDeHaeck/Postcard.git
$ git push -u origin master
```

Push an existing repository from the command line:

```
$ git remote add origin https://github.com/MathieuDeHaeck/RepoName.git
$ git push -u origin master
```

###Bitbucket

Set up your local directory:
```
$ mkdir /path/to/your/project
$ cd /path/to/your/project
$ git init
$ git remote add origin https://mathieudehaeck@bitbucket.org/mathieudehaeck/test.git
```

Create your first file, commit, and push:
```
$ echo "Mathieu De Haeck" >> contributors.txt
$ git add contributors.txt
$ git commit -m 'Initial commit with contributors'
$ git push -u origin master
```

###Git Client applications
- [GitHub app](https://mac.github.com/)
- [Source tree](http://www.sourcetreeapp.com/)
- [Tower](http://www.git-tower.com/)

###Diff Tools on OS X
[http://www.git-tower.com/blog/diff-tools-mac/](http://www.git-tower.com/blog/diff-tools-mac/)

***

#Sass (scss)

Official site: [http://sass-lang.com/](http://sass-lang.com/)
  

Playground: [Sassmeister](http://sassmeister.com/)

###Install Sass
1. Open terminal
2. Install sass ruby gem:
```
$ gem install sass
```
If you get an error message then it's likely you will need to use the sudo command to install the Sass gem. It would look like:
```
$ sudo gem install sass
```
3. Double-check if sass is correctly installed:
```
$ sass -v
```
or
```
$ sass --version
```
4. For more information or help:
```
$ sass -h
```
or
```
$ sass --help
```

###Compiling

Using terminal:
```
sass --watch .
```

***

#Compass

Official site: [http://compass-style.org/](http://compass-style.org/)

###Install compass

1. Open terminal
2. Update your system:
```
$ gem update --system
```
2. Install sass ruby gem:
```
$ gem install compass
```
If you get an error message then it's likely you will need to use the sudo command to install the Sass gem. It would look like:
```
$ sudo gem install compass
```
3. Double-check if sass is correctly installed:
```
$ compass --version
```

###Set up compass

[Compass online setup tool](http://compass-style.org/install/)

Create a new default compass project:
```
$ compass create myprojectname
```

Create a new custom directory project:
```
$ compass create myprojectname --sass-dir "sass" --css-dir "css" --javascripts-dir "js" --images-dir "img" --fonts-dir "fonts"
```

Add compass to an existing project (no compass starter stylesheets)
```
$ cd myproject
$ compass create --bare;
```
*If you don't using compass starter stylesheets, don't forget to import compass in your sass(scss) file:
```
@import "compass";
```

Configuration options in config.rb file.  

Example for development:

```
http_path = "/"
css_dir = "stylesheets"
sass_dir = "sass"
images_dir = "images"
javascripts_dir = "javascripts"
fonts_dir = "fonts"
output_style = :nested
environment = :development
color_output = false
preferred_syntax = :scss
```

Example for production:
```
http_path = "/"
css_dir = "stylesheets"
sass_dir = "sass"
images_dir = "images"
javascripts_dir = "javascripts"
fonts_dir = "fonts"
output_style = :compressed
environment = :production
color_output = false
preferred_syntax = :scss
```

###Compiling

Watch:
```
$ compass watch
```

Compile
```
$ compass compile fileName
```

Help
```
$ compass -h
```

***

#Bourbon
Official site: [http://bourbon.io/](http://bourbon.io/)

##Installation

Install the gem:
```
$ gem install bourbon
```

Install Bourbon into your project’s stylesheets directory by generating the bourbon folder:
```
$ bourbon install
```
###Other Commands

```
$ bourbon help
$ bourbon update
```

***

#File watchers & alternatives

- Command line
    * Ruby gems (Sass, Compass, Guard)
    * Task runners (Grunt, Gulp)
- Applications
    * [CodeKit (Paid) OS X](http://incident57.com/codekit/)
    * [Compass.app (Paid, Open Source) OS X, Windows, Linux](http://compass.kkbox.com/)
    * [Hammer (Paid) OS X](http://hammerformac.com/)
    * [Koala (Open Source) OS X, Windows, Linux](http://koala-app.com/)
    * [LiveReload (Paid, Open Source) OS X, Windows](http://livereload.com/)
    * [Mixture (Paid) OS X, Windows](http://mixture.io/)
    * [Prepros (Paid, Open Source) OS X, Windows](http://alphapixels.com/prepros/)
    * [Scout (Open Source) OS X, Windows](http://mhs.github.io/scout-app/)

#Guard & live reloading
[Github repo](https://github.com/guard/guard-livereload)
[Article](http://www.amazeelabs.com/de/node/136)

Install the gem:
```
$ sudo gem install compass compass-validator guard g
```

Create the guardFile:
```
$ nano Guardfile
```

Config the GuardFile:
```
group :development do
  guard :livereload do
    watch(%r{.+\.(css|js|html?)$})
  end

  if File.exists?("./config.rb")
    # Compile on start.
    puts `compass compile --time --quiet`
    guard :compass do
      watch(%r{(.*)\.s[ac]ss$})
    end
  end
end
```

Run guard on your project:
```
$ guard -i
```

***

#Bower
Official site: [http://bower.io/](http://bower.io/)

Install Bower:
```
$ npm install -g bower
```

Install packages:
```
$ bower install <package>
```

Search package:
```
$ bower search <package>
```

Save packages:
```
$ bower init
```

Install and save package
```
# install package and add it to bower.json dependencies
$ bower install <package> --save
# install package and add it to bower.json devDependencies
$ bower install <package> --save-dev
```

Register a package:
[http://bower.io/docs/creating-packages/#register](http://bower.io/docs/creating-packages/#register)

***

#Javascript taskrunners

##GruntJS

- Official site: [http://gruntjs.com/](http://gruntjs.com/)
- [Getting started](http://gruntjs.com/getting-started)
- [Grunt JavaScript Automation for the Lazy Developer](https://www.youtube.com/watch?v=bntNYzCrzvE)

###Install
Installing the Grunt Command Line Interface globally:
```
$ npm install -g grunt-cli
```

###Setting up the project
Create `package.json` file:
```
$ npm init
```

or create a blank file, for exmaple:

```
{
  "name": "my-project-name",
  "version": "0.1.0",
  "devDependencies": {
    "grunt": "~0.4.5",
    "grunt-contrib-jshint": "~0.10.0",
    "grunt-contrib-nodeunit": "~0.4.1",
    "grunt-contrib-uglify": "~0.5.0"
  }
}
```

Install Grunt in you project and save it in your devDependencies:
```
$ npm install grunt --save-dev
```

Other users on your project can do now `$ npm install` for installing the correct dependencies.

Create `Gruntfile.js` file, for example:
```
module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      build: {
        src: 'src/<%= pkg.name %>.js',
        dest: 'build/<%= pkg.name %>.min.js'
      }
    }
  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-uglify');

  // Default task(s).
  grunt.registerTask('default', ['uglify']);

};
```

Install a plugin:
```
$ npm install grunt-contrib-uglify --save-dev
```

[Sample Gruntfile](http://gruntjs.com/sample-gruntfile)

##GulpJS

- Official site: [http://gulpjs.com/](http://gulpjs.com/)
- [Getting started](https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md#getting-started)
- [Plugins](http://gulpjs.com/plugins/)
- [API documentation](https://github.com/gulpjs/gulp/blob/master/docs/API.md)
- [Setting up Gulp, Bower, Bootstrap Sass, & FontAwesome](http://ericlbarnes.com/setting-gulp-bower-bootstrap-sass-fontawesome/)
- [Learning Gulp video tutorials](https://www.youtube.com/playlist?list=PLLnpHn493BHE2RsdyUNpbiVn-cfuV7Fos)

Intsall gulp global:
```
$ npm install -g gulp
```

Create `package.json` file:
```
$ npm init
```

Install and save gulp in your project devDependencies:
```
$ npm install --save-dev gulp
```

Install and save a gulp plugin in devDependencies:
```
$ npm install --save-dev gulp-packageName
```

Create a `gulpfile.js` at the root of your project:
```
var gulp = require('gulp');
gulp.task('default', function() {
  // place code for your default task here
});

```
Run gulp:
```
$ gulp
```

***

#Yeoman

Official site: [http://yeoman.io/](http://yeoman.io/)

Install Yeoman:
```
$ npm install -g yo
```

###Generators

[http://yeoman.io/generators/](http://yeoman.io/generators/)

It's preferred to use the `$ yo` command line to in/uninstall or update generators.  

Otherwise you can use the `$ npm` command:

Install a generator:
```
$ npm install -g generator-angular
```

Uninstall a generator:
```
$ npm uninstall -g generator-angular
```

Update generator:
```
$ npm update -g generator-angular
```

Creating a generator: [http://yeoman.io/authoring/](http://yeoman.io/authoring/)  

Popular generators:

- [angular](https://github.com/yeoman/generator-angular)
- [webapp (Grunt)](https://github.com/robwierzbowski/generator-jekyllrb)
- [gulp-webapp](https://github.com/yeoman/generator-gulp-webapp)
- [jekyllrb](https://github.com/robwierzbowski/generator-jekyllrb)

***

#Extra

###PHP storm 8 Tips & Tricks
- file types
- language support
- watchers
- terminal
- grunt tasks
- shortcuts
- source control

###Where can i find all the node and ruby stuff on my computer ?

Node modules:
```
Usr/local/lib/node_modules
```

Ruby & gems
```
Usr/lib/ruby
```

###Feeds
- [The treehouse show](http://teamtreehouse.com/library/the-treehouse-show)

###Some good online tutorials:
- [Grunt & command line](https://www.youtube.com/watch?v=1tRAxraGl2g)
- [AngularJS](https://www.youtube.com/watch?v=i9MHigUZKEM)
- [Team treehouse](http://teamtreehouse.com/library)
- [Udemy](https://www.udemy.com/)
- [Lynda.com](http://www.lynda.com/subject/all)

###Handy trouble shooting links
- [How to Set File Permissions in Mac OS X](http://www.macinstruct.com/node/415)
- [How do I completely uninstall Node.js, and reinstall from beginning (Mac OS X)](http://stackoverflow.com/questions/11177954/how-do-i-completely-uninstall-node-js-and-reinstall-from-beginning-mac-os-x)
- [Making git “forget” about a file that was tracked but is now in .gitignore](http://stackoverflow.com/questions/1274057/making-git-forget-about-a-file-that-was-tracked-but-is-now-in-gitignore)
- [How do I recompile Bash to avoid Shellshock (the remote exploit CVE-2014-6271 and CVE-2014-7169)?](http://apple.stackexchange.com/questions/146849/how-do-i-recompile-bash-to-avoid-shellshock-the-remote-exploit-cve-2014-6271-an)