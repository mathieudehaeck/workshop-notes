Design voor web en mobile
=========================
Workshop by Mathieu De Haeck

###Do's
- Orde in de design source files
- Logische engelstalige en juiste benamingen van layers
- Werk altijd op een pixelgrid
- Probeer altijd waarden te nemen die deelbaar zijn door 2
- Werk altijd in de RGB kleurmodus
- Werk altijd met de juiste hexadecimale kleurcodes (volgend brand guidelines, indien nodig)
- Maak altijd gebruik van een layout grid zoals Bootstrap (voor webdesign)
- Bij het kiezen van (system) fonts altijd alternatieven meegeven
- Voor andere fonts steeds de licenties aankopen voor het webfont (via Ise)
- Denk altijd Mobile Firts (Wij doen dit nog steeds niet)
- Maak meerdere designs voor Mobile, tablet portrait en desktop
- Probeer altijd een flexibel responsive design te maken zonder al te veel terug te vallen op breakpoints
- Maak slimme en herbruikbare symbolen/layers, die makkelijk en snel aanpasbaar zijn
- Denk verder na: 
     * Wat als er meer als 5 menu items zijn ?
     * Wat met vertalingen van labels die meer karakters/plaats nodig hebben ?
     * Wat als er geen content beschikbaar is, weinig of zeer veel ?
     * Wat als mijn venster breder is als het design dat ik gemaakt heb ? (background images, content)
- Werk consistent (groottes, kleuren, layout, etc)
- Logo's vectorieel aanleveren (Liefst het hele design)

###Don'ts
- GEEN WAARDEN NA DE KOMMA !!! zowel voor positionering als afmetingen
- Speel niet vals met openingen in transparante afbeelding door er vlakken op te plaatsen met dezelfde achtergrond kleur
- Geen gelinkte afbeeldingen
- Geen outline fonts en de naam/type van het font er niet bij zetten
- Bestanden niet opmaken in CMYK !
- Geen complexe files met groups in groups in groups...
- 1001 lagen en paden op elkaar, clipping masks op gigantische afbeeldingen die dan nog meetsal gelinkt staan
- Geen overdreven zware bestanden aanleveren
- 20140809_design_finaal_versie2_vanWeetIkVeelWie.ai
- Steek geen elementen in het design, als je niet zeker weet of technisch haalbaar zijn
- Geen verstopte layers of honderden elementen buiten artboards zetten (die niet eens gebruikt worden)

### HTML & CSS
- http://www.w3schools.com/

### Werken met layoud grids en Frameworks
- Breakpoints & fluid design
- [KAN Blog post: Het simpele verschil tussen adaptive, responsive, liquid en static web design.](http://www.kandesign.com/nl/blog/single-nl/article/the-simple-difference-between-adaptive-responsive-liquid-and-static-web-design-we-know-what-we-pr/)

#### Frameworks & layout grids
- http://960.gs/ (oldschool)
- http://unsemantic.com/
- http://foundation.zurb.com/
- http://getbootstrap.com/
- http://isotope.metafizzy.co/
- http://salvattore.com/
- http://susy.oddbird.net/

#### Bootstrap 3
- http://getbootstrap.com/
- http://getbootstrap.com/examples/grid/
- http://getbootstrap.com/css/
- http://getbootstrap.com/components/
- http://getbootstrap.com/javascript/

### Glyphicons
- http://glyphsearch.com/
- http://getbootstrap.com/components/#glyphicons
- http://fortawesome.github.io/Font-Awesome/
- http://fontastic.me/

### Webfonts
- Vertical Rhythm
     * http://typecast.com/gallery/sherlock-holmes-baseline
     * http://24ways.org/2006/compose-to-a-vertical-rhythm/
- Mobile
     * http://typecast.com/blog/a-more-modern-scale-for-web-typography
     * http://iosfonts.com/
- Adobe Typekit - https://typekit.com/
- Google Fonts - https://www.google.com/fonts
- Fontsquirrel - http://www.fontsquirrel.com/fonts/list/hot_web
- Fonts.com - http://www.fonts.com/web-fonts

### Scherm dimensies
Bij Apple spreken ze altijd in punten en niet in pixels
- http://sebastien-gabriel.com/designers-guide-to-dpi/home
- http://www.paintcodeapp.com/news/ultimate-guide-to-iphone-resolutions

#### iOS/OSX
- @1x
- @2x Retina
- @3x Retina HD

#### Android
- ldpi (low) ~120dpi 75%
- mdpi (medium) ~160dpi 100% (iOS @1x)
- TVDPI 133%
- hdpi (high) ~240dpi 150%
- xhdpi (extra-high) ~320dpi 200% (iOS @2x)
- xxhdpi (extra-extra-high) ~480dpi 300% (iOS @3x)
- xxxhdpi (extra-extra-extra-high) ~640dpi 400%

### Touch targets
- iOS 44x44pt
- Android 48x48dp
- 60x60pt

### Icons
http://css-tricks.com/favicon-quiz/
http://teamtreehouse.com/library/the-treehouse-show/episode-112-icons-responsive-images-timesheets
https://developer.apple.com/library/ios/documentation/userexperience/conceptual/mobilehig/
http://developer.android.com/design/style/iconography.html
http://msdn.microsoft.com/en-us/library/ie/dn455106(v=vs.85).aspx

#### Tools
- http://www.kodlian.com/apps/icon-slate
- https://itunes.apple.com/us/app/asset-catalog-creator-app/id809625456?mt=12
- http://devrocket.uiparade.com/ (photoshop plugin)

### Software
- Adobe photoshop
- Adobe Illustrator
- Bohemian Sketch3

#### Adobe Photoshop
http://www.adobe.com/nl/products/photoshop.html

##### Gebruikers
- Fotografen
- Web designers
- Mobile app designers
- Software designers

##### Voordelen
- Developers hebben dit programma, en kunnen hier assest mee uit-slicen en exporteren
- Smart objects
- Actions
- Werken op pixels
- Grote community
- Zeer veel plugins, templates, GUI, actions, etc
- Werkt goed samen met Illustrator om smart object te bewerken

##### Nadelen
- Werken op een raster (kan gedeeltelijk opgevangen worden door smart objects)
- Slechte export (grote bestandsformaten)
- Geen artboards
- Guidelines wijken steeds van pixels af
- Export voor verschillende scherm dimensies

##### Resources
- https://dribbble.com/tags/psddd
- https://uistore.io/
- https://madebysource.com/
- http://guideguide.me/
- http://macrabbit.com/slicy/
- http://www.teehanlax.com/tools/iphone/
- https://dribbble.com/shots/1208992-Bootstrap-V3-0-0

#### Adobe Illustrator
http://www.adobe.com/nl/products/illustrator.html

##### Gebruikers
- Illustrators
- Logo designers
- Print designers (drukwerk)
- Product ontwikkelaars (technische tekeningen)

##### Voordelen
- Werken met vectoren
- Artboards
- Zeer goede tools voor figuren en vectorpaden te tekenen
- Ideaal om iconen en logo's te creëren
- Goede SVG export

##### Nadelen
- Zeer slechte export van assets (zware bestanden en kleur afwijkingen)
- Weinig tools, templates en andere voor web en mobile design
- Afstanden en afmetingen bepalen is zeer omslachtig
- 99% van de developers kunnen hier niet mee werken
- Laat de gebruiker makkelijk toe om:
    * Zeer zware source bestanden te genereren
    * Off pixel te werken x, y as en width, height values
    * In CMYK te werken !
    * Zeer complexe files met groups in groups in groups...
    * 1001 lagen en paden op elkaar, clipping masks op gigantische afbeeldingen die dan nog meetsal gelinkt staan
    * Ouline fonts waarvan nergens de naam, en type van het font bij staat

##### Resources
- http://bootstrapuikit.com/
- http://ikono.me/free-vector-ios7-ui-kit/
- http://mercury.io/blog/ios-8-illustrator-vector-ui-kit-update

#### Bohemian Sketch3
http://bohemiancoding.com/sketch/

##### Gebruikers
- Web designers
- Mobile app designers
- Software designers
- Front-end & iOS developers

##### Voordelen
- Goedkoop
- Uitgebreide documentatie
- API
- Grote community van designerd en developers!
- Native font renderign
- Sketch toolbox
- Zeer goede starter templates (die je zelf kan toevoegen of aanpassen)
- Global styles
- Symbols (herbruikbare elementen)
- De "canvas" werkt zoals die van Xcode
- Zeer goede export tool
- Grid tool
- Native mirror app voor mobile devices
- Makkijk om effecten te genereren
- Pages
- Autosave
- iCloud
- Illustrator files/objecten kunnen geopend op geïmporteerd worden (als deze correct zijn opgemaakt)

##### Nadelen
- Beperkte en soms omslachtige tools om figuren, iconen en logo's te creëren
- Vrij jong programma (2 jaar)
- Werkt enkel op OSX

##### Resources
- http://bohemiancoding.com/sketch/support/documentation/
- http://sketchtips.tumblr.com/
- http://sketchtoolbox.com/
- http://www.designyourway.net/blog/resources/sketch-plugins-that-will-improve-your-workflow/
- http://sketchshortcuts.com/
- http://hackingui.com/design/sketch-design/why-i-moved-to-sketch/
- http://www.sketchappsources.com/
- http://mdznr.roon.io/automatically-exporting-assets-from-sketch-into-xcode
- https://www.makerscabin.com/sketch/tips?/sketch/tips
- https://designcode.io/sketch
- https://medium.com/@jm_denis/what-is-new-in-sketch-3-4b92d8b25f3
- http://www.teehanlax.com/tools/iphone-sketch-app/
- http://bootstrapuikit.com/
- https://www.youtube.com/playlist?list=PLLnpHn493BHE6UIsdKYlS5zu-ZYvx22CS

##### Handige Plugins
- https://github.com/tadija/AEIconizer
- https://github.com/timuric/Content-generator-sketch-plugin
- https://github.com/jimrutherford/UIColor-Category-Generator
- https://github.com/ddwht/sketch-dynamic-button
- https://github.com/utom/sketch-measure
- https://github.com/GeertWille/sketch-export-assets
- https://github.com/brandonbeecroft/Lorem-Ipsum-Plugin-for-Sketch



### Userflow & design patterns
- http://www.mobile-patterns.com/
- http://www.pttrns.com/
- https://developers.google.com/web/fundamentals/layouts/rwd-patterns/

### Wireframes, prototypes & tools
- https://www.fluidui.com/
- http://mockuphone.com/
- http://appicontest.com/
- https://app.io/

### Design Guidelines
- http://www.google.com/design/
- https://developer.apple.com/library/ios/documentation/userexperience/conceptual/mobilehig/
- https://developer.android.com/design/index.html
- https://dev.windows.com/en-us/design
- https://www.facebookbrand.com/
- https://about.twitter.com/press/brand-assets
- https://help.instagram.com/304689166306603
- http://developer.linkedin.com/documents/branding-guidelines
- https://developers.google.com/youtube/branding_guidelines
- http://vimeo.com/about/brand_guidelines

### Handige design Tools
- Sip color picker - http://www.macupdate.com/app/mac/42337/sip
- icons8 - http://icons8.com/app/
- LittleIpsum - http://littleipsum.com/
- Skala preview - http://bjango.com/mac/skalapreview/

### Newsletters, feeds & inspiration
- https://dribbble.com/
- https://www.behance.net/
- http://www.smashingmagazine.com/
- https://www.cocoacontrols.com/
- http://iosdevweekly.com/
- http://teamtreehouse.com/library/the-treehouse-show
- https://plus.google.com/communities
- http://ios.devtools.me/tag?id=typography

### Last but not least
- https://github.com/MathieuDeHaeck/website-list