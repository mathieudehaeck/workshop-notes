Typo3 6.2.0 LTS & speciality plugin documentation
=================================================
By Niels Verhesen, Mathieu De Haeck

#Setup a new TYPO3 website

1. Install TYPO3 6.2 LTS Core outside website root folder <br/>
  http://typo3.org/download/

2. Open Terminal and create a new project root folder for your new website
```
mkdir HelloWorld
```

3. Go inside your project root folder
```
cd HelloWorld/
```

4. Inside website root folder create symlinks to core installation  <br/>
Make sure the version in your path is the same as the TYPO3 version you downloaded
```
ln -s ../typo3_src-6.2.6 typo3_src
ln -s typo3_src/index.php index.php
ln -s typo3_src/typo3 typo3
```

5. Create a file named FIRST_INSTALL
```
touch FIRST_INSTALL
```

6. Create a virtual host for your new website
7. Access your newly created virtual host and complete the installation.

  1. username: root
  2. password: root
  3. host: localhost
  4. Create a new database
  5. backend username
  6. backend password
  7. Make sure to uncheck the checkbox to install pre-configured distributions.
  <br/><br/>

8. Login to the backend of your site and install the following plugins, using the Extention Manager:

  - Flux - 7.1.0
  - VHS - 2.1.2
  - RealUrl - 1.12.8
  - Fluidcontent - 4.1.0
  - Fluidpages - 3.1.1
  - Fluidcontent_bootstrap - 3.0.0
  <br/><br/>

9. Download the 'speciality'-plugin from the kandesign bitbucket repository and install it.
10. Create a shortcut root page, and create a TypoScript template to the root level.

11. In constants add the following line:
```
<INCLUDE_TYPOSCRIPT: source="FILE: EXT:speciality/Configuration/TypoScript/constants.txt">
```

12. In setup add the following line:
```
<INCLUDE_TYPOSCRIPT: source="FILE: EXT:speciality/Configuration/TypoScript/setup.txt">
```

13. Create a new page, for example "Home" and select a page layout

14. View the page in your browser, the page shows the selected template you selected in page layouts

#Using the speciality plugin

There are three important folders inside this plugin:

1.Configuration

In here you can find the basic TypoScript configuration for your new website. You can add all your additional TypoScript configuration inside these files or overrule the existing config in the backend.

2.Resources/Public

Inside this folder you can find all the assets (CSS, JS-libraries, images, icons, fonts, ...)

3.Resources/Private

Inside this folder you can find all the website layouts, templates and partials. This is where all the templating happens! There are a few default layouts and templates which you can use to make your own.
If templating is done right, in the backend in page properties you can choose your created page template within the tabpanel 'Page Layouts'.

##Test your local website in Internet Explorer using a virtual machine

1. Open pararlells and navigate in the folder to notepad.
Computer > Local Disk (C) > Windows > Notepad

2. Right click on the notepad icon, and select "Open as administrator"
3. In Notepad file > open and navigate to the hosts file *don't forget to select "All files in the selectbox below"
Computer > Local Disk (C) > Windows > System32 > drivers > etc > hosts

4. Paste your MAC IP adress, tab, and paste your vhost (don't use any ports)
192.168.1.145	yoursite.local

5. Save the file and visit your vhost in your vm browser: http://yoursite.local:8888